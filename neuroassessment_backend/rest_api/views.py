from json import loads

from django.shortcuts import render

from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from neuroassessment_backend.polls.models import Poll, Section, Question, Answer
from neuroassessment_backend.rest_api.serializers import PollSerializer, AnswerSerializer

# Create your views here.


class PollView(RetrieveAPIView):
    lookup_field = 'slug'
    serializer_class = PollSerializer
    queryset = Poll.objects.filter(active=True)


class QuestionView(APIView):
    def get(self, request, slug, user_key):
        poll = Poll.objects.get(slug=slug)
        if not poll.allow_all:
            if user_key not in poll.allowed_ids:
                return Response(status=HTTP_400_BAD_REQUEST)
        a, created = Answer.objects.get_or_create(poll=poll, user_key=user_key)
        a.save()
        return Response(a.get_question())

    def post(self, request, slug, user_key):
        poll = Poll.objects.get(slug=slug)
        a = Answer.objects.get(poll=poll, user_key=user_key)
        body = loads(request.body)
        question_pk = body['pk']
        answer = body['answer']
        result = a.set_answer(question_pk, answer)
        return Response(a.get_question())


class ResultView(RetrieveAPIView):
    lookup_field = 'result_key'
    serializer_class = AnswerSerializer
    queryset = Answer.objects.all()
