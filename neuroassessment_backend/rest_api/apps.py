from django.apps import AppConfig


class RestApiConfig(AppConfig):
    name = 'neuroassessment_backend.rest_api'
