from django.urls import path

from neuroassessment_backend.rest_api.views import PollView, QuestionView, ResultView

app_name = "rest_api"
urlpatterns = [
    path("<str:slug>/", view=PollView.as_view(), name="poll"),
    path("<str:slug>/question/<str:user_key>", view=QuestionView.as_view(), name="question"),
    path("result/<str:result_key>", view=ResultView.as_view(), name="result"),
]
