from rest_framework import serializers

from neuroassessment_backend.polls.models import Poll, Answer


class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = ('name', 'greeting_text')


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('result', 'result_export', 'poll_data')
