from django.contrib import admin, messages
from django.contrib.postgres import fields
from django.utils.html import format_html

from django_json_widget.widgets import JSONEditorWidget
from import_export import resources, widgets
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field
from import_export.admin import ExportActionMixin

from neuroassessment_backend.polls.models import Question, Section, Poll, Answer, UploadHandler

# Register your models here.


class AnswerResource(resources.ModelResource):
    Math = Field()

    def dehydrate_Math(self, instance):
        if instance.poll_data:
            for q in instance.poll_data:
                if q.get('text', None) == "Успеваемость по МАТЕМАТИКЕ":
                    return str(q.get('answer', None))
        else:
            return ''

    English = Field()

    def dehydrate_English(self, instance):
        if instance.poll_data:
            for q in instance.poll_data:
                if q.get('text', None) == "Успеваемость по АНГЛИЙСКОМУ ЯЗЫКУ":
                    return str(q.get('answer', None))
        else:
            return ''

    Feelings = Field()

    def dehydrate_Feelings(self, instance):
        if instance.poll_data:
            for q in instance.poll_data:
                if q.get('text', None) == "Внимателен к чувствам других людей":
                    return str(q.get('answer', None))
        else:
            return ''

    Hyperactive = Field()

    def dehydrate_Hyperactive(self, instance):
        if instance.poll_data:
            for q in instance.poll_data:
                if q.get('text', None) == "Неугомонный, слишком активный, не может долго оставаться спокойным":
                    return str(q.get('answer', None))
        else:
            return ''

    B_sum = Field()

    def dehydrate_B_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('B_sum', None)
        else:
            return ''

    IN_sum = Field()

    def dehydrate_IN_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('IN_sum', None)
        else:
            return ''

    IO_sum = Field()

    def dehydrate_IO_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('IO_sum', None)
        else:
            return ''

    IR_sum = Field()

    def dehydrate_IR_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('IR_sum', None)
        else:
            return ''

    ML_sum = Field()

    def dehydrate_ML_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('ML_sum', None)
        else:
            return ''

    RC_sum = Field()

    def dehydrate_RC_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('RC_sum', None)
        else:
            return ''

    RF_sum = Field()

    def dehydrate_RF_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('RF_sum', None)
        else:
            return ''

    RL_sum = Field()

    def dehydrate_RL_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('RL_sum', None)
        else:
            return ''

    SE_sum = Field()

    def dehydrate_SE_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SE_sum', None)
        else:
            return ''

    TA_sum = Field()

    def dehydrate_TA_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('TA_sum', None)
        else:
            return ''

    TE_sum = Field()

    def dehydrate_TE_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('TE_sum', None)
        else:
            return ''

    VR_sum = Field()

    def dehydrate_VR_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('VR_sum', None)
        else:
            return ''

    BCH_sum = Field()

    def dehydrate_BCH_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('BCH_sum', None)
        else:
            return ''

    CYB_sum = Field()

    def dehydrate_CYB_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('CYB_sum', None)
        else:
            return ''

    IOT_sum = Field()

    def dehydrate_IOT_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('IOT_sum', None)
        else:
            return ''

    SPE_sum = Field()

    def dehydrate_SPE_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SPE_sum', None)
        else:
            return ''

    SPM_sum = Field()

    def dehydrate_SPM_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SPM_sum', None)
        else:
            return ''

    SPP_sum = Field()

    def dehydrate_SPP_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SPP_sum', None)
        else:
            return ''

    SRF_sum = Field()

    def dehydrate_SRF_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SRF_sum', None)
        else:
            return ''

    SRL_sum = Field()

    def dehydrate_SRL_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SRL_sum', None)
        else:
            return ''

    SRT_sum = Field()

    def dehydrate_SRT_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SRT_sum', None)
        else:
            return ''

    TLB_sum = Field()

    def dehydrate_TLB_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('TLB_sum', None)
        else:
            return ''

    TLD_sum = Field()

    def dehydrate_TLD_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('TLD_sum', None)
        else:
            return ''

    B_average = Field()

    def dehydrate_B_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('B_average', None)
        else:
            return ''

    B_quartile = Field()

    def dehydrate_B_quartile(self, instance):
        if instance.result_export:
            return instance.result_export.get('B_quartile', None)
        else:
            return ''

    IN_average = Field()

    def dehydrate_IN_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('IN_average', None)
        else:
            return ''

    IO_average = Field()

    def dehydrate_IO_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('IO_average', None)
        else:
            return ''

    IR_average = Field()

    def dehydrate_IR_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('IR_average', None)
        else:
            return ''

    ML_average = Field()

    def dehydrate_ML_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('ML_average', None)
        else:
            return ''

    RC_average = Field()

    def dehydrate_RC_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('RC_average', None)
        else:
            return ''

    RF_average = Field()

    def dehydrate_RF_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('RF_average', None)
        else:
            return ''

    RL_average = Field()

    def dehydrate_RL_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('RL_average', None)
        else:
            return ''

    SE_average = Field()

    def dehydrate_SE_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SE_average', None)
        else:
            return ''

    TA_average = Field()

    def dehydrate_TA_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('TA_average', None)
        else:
            return ''

    TE_average = Field()

    def dehydrate_TE_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('TE_average', None)
        else:
            return ''

    VR_average = Field()

    def dehydrate_VR_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('VR_average', None)
        else:
            return ''

    BCH_average = Field()

    def dehydrate_BCH_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('BCH_average', None)
        else:
            return ''

    CYB_average = Field()

    def dehydrate_CYB_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('CYB_average', None)
        else:
            return ''

    IOT_average = Field()

    def dehydrate_IOT_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('IOT_average', None)
        else:
            return ''

    ML_quartile = Field()

    def dehydrate_ML_quartile(self, instance):
        if instance.result_export:
            return instance.result_export.get('ML_quartile', None)
        else:
            return ''

    SPE_average = Field()

    def dehydrate_SPE_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SPE_average', None)
        else:
            return ''

    SPM_average = Field()

    def dehydrate_SPM_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SPM_average', None)
        else:
            return ''

    SPP_average = Field()

    def dehydrate_SPP_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SPP_average', None)
        else:
            return ''

    SRF_average = Field()

    def dehydrate_SRF_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SRF_average', None)
        else:
            return ''

    SRL_average = Field()

    def dehydrate_SRL_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SRL_average', None)
        else:
            return ''

    SRT_average = Field()

    def dehydrate_SRT_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SRT_average', None)
        else:
            return ''

    TLB_average = Field()

    def dehydrate_TLB_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('TLB_average', None)
        else:
            return ''

    TLD_average = Field()

    def dehydrate_TLD_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('TLD_average', None)
        else:
            return ''

    VR_quartile = Field()

    def dehydrate_VR_quartile(self, instance):
        if instance.result_export:
            return instance.result_export.get('VR_quartile', None)
        else:
            return ''

    BCH_quartile = Field()

    def dehydrate_BCH_quartile(self, instance):
        if instance.result_export:
            return instance.result_export.get('BCH_quartile', None)
        else:
            return ''

    CYB_quartile = Field()

    def dehydrate_CYB_quartile(self, instance):
        if instance.result_export:
            return instance.result_export.get('CYB_quartile', None)
        else:
            return ''

    IOT_quartile = Field()

    def dehydrate_IOT_quartile(self, instance):
        if instance.result_export:
            return instance.result_export.get('IOT_quartile', None)
        else:
            return ''

    IN_percentile = Field()

    def dehydrate_IN_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('IN_percentile', None)
        else:
            return ''

    IO_percentile = Field()

    def dehydrate_IO_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('IO_percentile', None)
        else:
            return ''

    IR_percentile = Field()

    def dehydrate_IR_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('IR_percentile', None)
        else:
            return ''

    RC_percentile = Field()

    def dehydrate_RC_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('RC_percentile', None)
        else:
            return ''

    RF_percentile = Field()

    def dehydrate_RF_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('RF_percentile', None)
        else:
            return ''

    RL_percentile = Field()

    def dehydrate_RL_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('RL_percentile', None)
        else:
            return ''

    SE_percentile = Field()

    def dehydrate_SE_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('SE_percentile', None)
        else:
            return ''

    TA_percentile = Field()

    def dehydrate_TA_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('TA_percentile', None)
        else:
            return ''

    TE_percentile = Field()

    def dehydrate_TE_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('TE_percentile', None)
        else:
            return ''

    SR_percentile = Field()

    def dehydrate_SR_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('SR_percentile', None)
        else:
            return ''

    SR_sum = Field()

    def dehydrate_SR_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SR_sum', None)
        else:
            return ''

    SR_average = Field()

    def dehydrate_SR_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SR_average', None)
        else:
            return ''

    SP_percentile = Field()

    def dehydrate_SP_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('SP_percentile', None)
        else:
            return ''

    SP_sum = Field()

    def dehydrate_SP_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('SP_sum', None)
        else:
            return ''

    SP_average = Field()

    def dehydrate_SP_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('SP_average', None)
        else:
            return ''

    TL_percentile = Field()

    def dehydrate_TL_percentile(self, instance):
        if instance.result_export:
            return instance.result_export.get('TL_percentile', None)
        else:
            return ''

    TL_sum = Field()

    def dehydrate_TL_sum(self, instance):
        if instance.result_export:
            return instance.result_export.get('TL_sum', None)
        else:
            return ''

    TL_average = Field()

    def dehydrate_TL_average(self, instance):
        if instance.result_export:
            return instance.result_export.get('TL_average', None)
        else:
            return ''

    class Meta:
        model = Answer
        exclude = ('result', 'result_export', 'poll_data')


def create_copy(modeladmin, request, queryset):
    for poll in queryset:
        result = poll.duplicate()
        messages.add_message(request, messages.INFO, result)


create_copy.short_description = 'Создать копию опроса'


def force_send_webhook(modeladmin, request, queryset):
    for answer in queryset:
        result = answer.send_webhook()
        messages.add_message(request, messages.INFO, result)


force_send_webhook.short_description = 'Отправить данные через вебхук'


def force_refresh_progress(modeladmin, request, queryset):
    for answer in queryset:
        result = answer.get_question()
        messages.add_message(request, messages.INFO, result)


force_refresh_progress.short_description = 'Обновить расчет прогресса'


def force_calculate_result(modeladmin, request, queryset):
    for answer in queryset:
        result = answer.calculate_result()
        messages.add_message(request, messages.INFO, result)


force_calculate_result.short_description = 'Обновить результат'


def force_send_invitation(modeladmin, request, queryset):
    for answer in queryset:
        result = answer.send_invitation()
        messages.add_message(request, messages.INFO, result)


force_send_invitation.short_description = 'Отправить приглашение'


class PollAdmin(admin.ModelAdmin):
    list_fields = ('name', 'slug', 'active')
    list_filter = ('active', )

    actions = [
        create_copy,
    ]

    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget},
    }


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('section', 'type', 'text', 'active', 'weight')
    list_filter = ('section__poll', 'section', 'type', 'active')
    ordering = ('section__poll', '-section__order', '-weight', )

    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget},
    }


class SectionAdmin(admin.ModelAdmin):
    ordering = ('poll', '-order', )
    list_display = ('poll', 'name', 'active', 'order', 'random_order')


class AnswerAdmin(ImportExportModelAdmin):
    list_display = ('user_key', 'poll', 'finished', 'progress', 'start', 'finish', 'result_url')
    readonly_fields = ('result_url', )
    list_filter = ('poll', )
    resource_class = AnswerResource

    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget},
    }

    actions = [
        force_refresh_progress,
        force_send_webhook,
        force_calculate_result,
        force_send_invitation,
    ]

    def result_url(self, obj):
        return format_html("<a href='{url}' target='_blank'>{url}</a>", url=obj.poll.result_template + obj.result_key)

    # result_url.mark_safe = True


class UploadHandlerAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'progress', 'log')
    list_display = ('poll', 'created', 'progress')

admin.site.register(Question, QuestionAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(Poll, PollAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(UploadHandler, UploadHandlerAdmin)
