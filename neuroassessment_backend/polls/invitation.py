DEFAULT_INVITATION = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;height:100%;font-family:georgia, times, 'times new roman', serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta name="x-apple-disable-message-reformatting">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="telephone=no" name="format-detection">
  <title>Ваша персональная ссылка на SDI, Skolkovo Digital Index</title>
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
  <style type="text/css">
@media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:16px!important } h1 { font-size:30px!important; text-align:center } h2 { font-size:26px!important; text-align:center } h3 { font-size:20px!important; text-align:center } h1 a { font-size:30px!important } h2 a { font-size:26px!important } h3 a { font-size:20px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:20px!important; display:block!important; border-width:10px 0px 10px 0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important; padding-bottom:10px } }
#outlook a {
	padding:0;
}
.ExternalClass {
	width:100%;
}
.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
	line-height:100%;
}
span.MsoHyperlink {
	mso-style-priority:100!important;
	text-decoration:underline!important;
}
.es-button {
	mso-style-priority:100!important;
	text-decoration:none!important;
}
a[x-apple-data-detectors=true] {
	color:inherit!important;
	text-decoration:none!important;
}
.es-desk-hidden {
	display:none;
	float:left;
	overflow:hidden;
	width:0;
	max-height:0;
	line-height:0;
	mso-hide:all;
}
</style>
 </head>
 <body style="width:100%;height:100%;font-family:georgia, times, 'times new roman', serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
  <span style="display:none !important;font-size:0px;line-height:0;color:#FFFFFF;visibility:hidden;opacity:0;height:0;width:0;mso-hide:all;">Мы рады пригласить вас к участию в исследовании цифровой восприимчивости. &nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌</span>
  <div class="es-wrapper-color" style="background-color:#F6F6F6;">
   <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#f6f6f6"></v:fill>
			</v:background>
		<![endif]-->
   <table cellpadding="0" cellspacing="0" class="es-wrapper" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;">
     <tr style="border-collapse:collapse;">
      <td valign="top" " style="padding:0;Margin:0;">
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
         <tr style="border-collapse:collapse;">
          <td align="center" style="padding:0;Margin:0;">
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="610" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;">
             <tr class="es-mobile-hidden" style="border-collapse:collapse;">
              <td align="left" style="Margin:0;padding-top:10px;padding-bottom:10px;padding-left:40px;padding-right:40px;">
               <!--[if mso]><table width="530" cellpadding="0" cellspacing="0"><tr><td width="251" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                 <tr style="border-collapse:collapse;">
                  <td width="251" class="es-m-p20b" align="left" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                     <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;Margin:0;font-size:0px;"><img class="adapt-img" src="https://xfh.stripocdn.email/content/guids/CABINET_1266ba5d432a803f8be8aa35e392fa0f/images/11581590163902795.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="115"></td>
                     </tr>
                   </table></td>
                 </tr>
               </table>
               <!--[if mso]></td><td width="20"></td><td width="259" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;">
                 <tr style="border-collapse:collapse;">
                  <td width="259" align="left" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                     <tr style="border-collapse:collapse;">
                      <td align="right" style="padding:0;Margin:0;font-size:0px;"><img class="adapt-img" src="https://xfh.stripocdn.email/content/guids/CABINET_1266ba5d432a803f8be8aa35e392fa0f/images/57371590164049933.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="100"></td>
                     </tr>
                   </table></td>
                 </tr>
               </table>
               <!--[if mso]></td></tr></table><![endif]--></td>
             </tr>
             <tr style="border-collapse:collapse;">
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                 <tr style="border-collapse:collapse;">
                  <td width="570" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                     <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;Margin:0;padding-bottom:10px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:16.8px;color:#333333;">Добрый день! </p></td>
                     </tr>
                     <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;Margin:0;padding-bottom:10px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:17px;color:#333333;">Мы рады пригласить вас к участию в исследовании цифровой восприимчивости.&nbsp;</p></td>
                     </tr>
                     <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;Margin:0;padding-bottom:10px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:17px;color:#333333;">Ваша персональная ссылка на SDI, Skolkovo Digital Index:<br><strong><a target="_blank" href="https://digital-index.skolkovo.ru/#/?poll={{ poll.slug }}&id={{ user_key }}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;">https://digital-index.skolkovo.ru/#/?poll={{ poll.slug }}&amp;id={{ user_key }}</a></strong></p></td>
                     </tr>
                     <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;Margin:0;padding-bottom:10px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:17px;color:#333333;">Общее время тестирования: 30-45 минут. Вы можете делать перерывы в процессе прохождения теста. Используйте ссылку для возвращения к тесту.</p></td>
                     </tr>
                   </table></td>
                 </tr>
               </table></td>
             </tr>
             <tr style="border-collapse:collapse;">
              <td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:20px;padding-right:20px;">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                 <tr style="border-collapse:collapse;">
                  <td width="570" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                     <tr style="border-collapse:collapse;">
                      <td align="center" style="Margin:0;padding-top:15px;padding-bottom:15px;padding-left:20px;padding-right:20px;font-size:0px;">
                       <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                         <tr style="border-collapse:collapse;">
                          <td style="padding:0;Margin:0px;border-bottom:1px dotted #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td>
                         </tr>
                       </table></td>
                     </tr>
                     <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:17px;color:#333333;">Если у вас возникнут сложности, пожалуйста, обратитесь к нашему менеджеру:</p></td>
                     </tr>
                   </table></td>
                 </tr>
                 <tr style="border-collapse:collapse;">
                  <td width="570" align="center" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                     <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;Margin:0;padding-top:10px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:14px;color:#333333;font-weight:bold;">Ксения Галкина</p></td>
                     </tr>
                     <tr style="border-collapse:collapse;">
                      <td align="left" class="es-m-txt-l" style="padding:0;Margin:0;padding-top:5px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:14px;color:#333333;">+79067605922</p></td>
                     </tr>
                     <tr style="border-collapse:collapse;">
                      <td class="es-m-txt-l" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;font-size:0px;">
                       <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                         <tr style="border-collapse:collapse;">
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:10px;"><a target="_blank" href="https://api.whatsapp.com/send?phone=79067605922" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;"><img title="Whatsapp" src="https://xfh.stripocdn.email/content/assets/img/messenger-icons/logo-colored/whatsapp-logo-colored.png" alt="Whatsapp" width="24" height="24" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td>
                          <td valign="top" align="center" style="padding:0;Margin:0;"><a target="_blank" href="mailto:Ksenia_Galkina@skolkovo.ru" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;"><img title="Email" src="https://xfh.stripocdn.email/content/assets/img/other-icons/logo-colored/mail-logo-colored.png" alt="Email" width="24" height="24" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td>
                         </tr>
                       </table></td>
                     </tr>
                     <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;Margin:0;padding-top:20px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:17px;color:#333333;">С уважением,<br>Центр executive-коучинга, развития и карьеры<br>Московской&nbsp;школы&nbsp;управления СКОЛКОВО<br><a target="_blank" href="https://coaching.skolkovo.ru/ru/coaching/about/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;">www.coaching.skolkovo.ru</a></p></td>
                     </tr>
                   </table></td>
                 </tr>
               </table></td>
             </tr>
             <tr style="border-collapse:collapse;">
              <td align="left" style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                 <tr style="border-collapse:collapse;">
                  <td width="570" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                     <tr style="border-collapse:collapse;">
                      <td align="center" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;font-size:0px;">
                       <table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                         <tr style="border-collapse:collapse;">
                          <td align="center" valign="top" style="padding:0;Margin:0;padding-right:5px;"><a target="_blank" href="http://www.facebook.com/skolkovo" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;"><img src="https://xfh.stripocdn.email/content/assets/img/social-icons/logo-colored-bordered/facebook-logo-colored-bordered.png" alt="Fb" title="Facebook" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td>
                          <td align="center" valign="top" style="padding:0;Margin:0;padding-right:5px;"><a target="_blank" href="https://vk.com/skolkovoschool" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;"><img src="https://xfh.stripocdn.email/content/assets/img/social-icons/logo-colored-bordered/vk-logo-colored-bordered.png" alt="VK" title="Vkontakte" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td>
                          <td align="center" valign="top" style="padding:0;Margin:0;padding-right:5px;"><a target="_blank" href="http://twitter.com/skolkovo" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;"><img src="https://xfh.stripocdn.email/content/assets/img/social-icons/logo-colored-bordered/twitter-logo-colored-bordered.png" alt="Tw" title="Twitter" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td>
                          <td align="center" valign="top" style="padding:0;Margin:0;padding-right:5px;"><a target="_blank" href="http://instagram.com/skolkovoschool" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;"><img src="https://xfh.stripocdn.email/content/assets/img/social-icons/logo-colored-bordered/instagram-logo-colored-bordered.png" alt="Ig" title="Instagram" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td>
                          <td align="center" valign="top" style="padding:0;Margin:0;padding-right:5px;"><a target="_blank" href="https://www.youtube.com/user/skolkovoschool/videos" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;"><img src="https://xfh.stripocdn.email/content/assets/img/social-icons/logo-colored-bordered/youtube-logo-colored-bordered.png" alt="Yt" title="Youtube" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td>
                          <td align="center" valign="top" style="padding:0;Margin:0;"><a target="_blank" href="https://soundcloud.com/skolkovo" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;"><img src="https://xfh.stripocdn.email/content/assets/img/social-icons/logo-colored-bordered/soundcloud-logo-colored-bordered.png" alt="Sc" title="Soundcloud" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td>
                         </tr>
                       </table></td>
                     </tr>
                     <tr style="border-collapse:collapse;">
                      <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:17px;color:#333333;">Московская школа управления СКОЛКОВО</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:17px;color:#333333;">Московская область, Одинцовский район, Сколково, ул.&nbsp;Новая,&nbsp;100</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:georgia, times, 'times new roman', serif;line-height:17px;color:#333333;">+7 495 539 30 03,&nbsp;<a target="_blank" href="mailto:info@skolkovo.ru" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:georgia, times, 'times new roman', serif;font-size:14px;text-decoration:underline;color:#1376C8;">info@skolkovo.ru</a></p></td>
                     </tr>
                   </table></td>
                 </tr>
               </table></td>
             </tr>
           </table></td>
         </tr>
       </table></td>
     </tr>
   </table>
  </div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;margin:0px;padding:0px;border:0px;width:36pt;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;margin:0px;padding:0px;border:0px;width:36pt;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;margin:0px;padding:0px;border:0px;width:36pt;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;"></div>
  <div style="position:absolute;left:-9999px;top:-9999px;margin:0px;padding:0px;border:0px;width:54pt;"></div>
 </body>
</html>
"""
