from requests import post
from json import dumps
import uuid
import copy
import pandas as pd

from django.db import models
from django.utils.crypto import get_random_string
from django .contrib.postgres.fields import JSONField
from django.utils.timezone import now
from django.core.mail import send_mail, EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.template import Context, Template
from django.db.models.signals import post_save
from django.dispatch import receiver

from .invitation import DEFAULT_INVITATION
from .percentile import *
# Create your models here.


specials = {
            'SP': ['SPE', 'SPM', 'SPP'],
            'SR': ['SRF', 'SRT', 'SRL'],
            'TL': ['TLB', 'TLD'],
        }


class Question(models.Model):
    TYPE_CHOICES = (
        ("TextInput", "Text Input"),
        ("StringInput", "String Input"),
        ("Select", "Select dropdown"),
        ("Rating", "Rating selector"),
        ("PictureSelect", "Picture selection"),
        ("SinglePictureSelect", "Single picture selection"),
        ("SingleSelect", "Single selection"),
        ("MultipleSelect", "Multiple selection"),
        ("MultipleSelectWithOther", "Multiple selection with custom text"),
    )
    type = models.CharField("Тип вопроса", max_length=50, choices=TYPE_CHOICES)
    text = models.CharField('Текст вопроса', max_length=500)
    use_html = models.BooleanField('Использовать html-версию вопроса для отображения', default=False)
    html_text = models.TextField('Текст вопроса в html', blank=True, null=True)
    answering_time = models.IntegerField('Время на запись ответа в секундах', default=120)
    section = models.ForeignKey('Section', verbose_name='Секция вопросов',
                                on_delete=models.PROTECT, related_name='questions',
                                blank=True, null=True)
    active = models.BooleanField('Активен', default=True)
    weight = models.IntegerField('Вес', default=0)
    details = JSONField('Данные вопроса', blank=True, null=True)

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = "Вопросы"
        ordering = ['weight', ]

    def __str__(self):
        return self.text


class Section(models.Model):
    name = models.CharField('Название секции', max_length=100)
    active = models.BooleanField('Активна', default=True)
    order = models.IntegerField('Порядок секции в опросе', default=1)
    poll = models.ForeignKey('Poll', verbose_name='Опрос', on_delete=models.CASCADE,
                             related_name='sections', blank=True, null=True)
    greeting_text = models.TextField('Текст приветствия', max_length=50000, blank=True, null=True)
    random_order = models.BooleanField('Перемешать вопросы', default=False)

    class Meta:
        verbose_name = 'Секция вопросов'
        verbose_name_plural = "Секции вопросов"
        ordering = ['order', ]

    def __str__(self):
        return "{0} | {1}".format(self.poll.name, self.name)


class Poll(models.Model):
    name = models.CharField('Название опроса', max_length=100)
    slug = models.SlugField(verbose_name='Slug опроса', blank=True, null=True)
    active = models.BooleanField('Активен', default=False)
    invitation = models.TextField('Письмо-приглашение', max_length=500000, blank=True, null=True,
                                  default=DEFAULT_INVITATION, help_text='Шаблон письма-приглашения к прохождению теста')
    greeting_text = models.TextField('Текст приветствия', max_length=50000, blank=True, null=True)
    finish_text = models.TextField('Текст при завершении', max_length=50000, blank=True, null=True)
    result_template = models.URLField('Шаблон отображения результата', blank=True, null=True)
    results_webhook = models.URLField('URL для отправки результатов', blank=True, null=True,
                                      help_text='Можно использовать переменные: id пользователя {id}, слаг опроса '
                                                '{slug} и название опроса {name}')
    webhook_auth_token = models.CharField('Токен авторизации при отправке результатов', max_length=100,
                                          blank=True, null=True, help_text='Передается в хедере Authorization')
    allow_all = models.BooleanField('Разрешать прохождение с любым id', default=True,
                                    help_text='Если не отмечена - для прохождения надо иметь id из списка доступных')
    allowed_ids = JSONField('Допустимые id', blank=True, null=True)

    class Meta:
        verbose_name = 'Опрос'
        verbose_name_plural = "Опросы"
        ordering = ['name', 'pk']

    def __str__(self):
        return self.name

    def duplicate(self):
        poll_copy = copy.copy(self)
        poll_copy.id = None
        poll_copy.slug = self.slug + '-copy'
        poll_copy.name = self.name + ' копия'
        poll_copy.save()
        for section in self.sections.all():
            section_copy = copy.copy(section)
            section_copy.id = None
            section_copy.poll = poll_copy
            section_copy.save()
            for question in section.questions.all():
                question_copy = copy.copy(question)
                question_copy.id = None
                question_copy.section = section_copy
                question_copy.save()
        return poll_copy.id


class Answer(models.Model):
    poll = models.ForeignKey('Poll', verbose_name="Опрос", on_delete=models.PROTECT)
    poll_data = JSONField('Данные вопросов', blank=True, null=True)
    questions = JSONField('Вопросы', blank=True, null=True)
    question_count = models.IntegerField('Количество вопросов', default=0)
    result = JSONField('Результаты', blank=True, null=True)
    result_export = JSONField('Результаты для экспортирования', blank=True, null=True)
    result_key = models.CharField('Токен доступа к результатам', unique=True, null=True, blank=True, max_length=20)
    user_key = models.CharField('Ключ пользователя', max_length=64)
    email = models.EmailField('Адрес email', blank=True, null=True)
    progress = models.IntegerField('Прогресс прохождения', default=0)
    result_link = models.URLField('Ссылка на результат', blank=True, null=True)
    start = models.DateTimeField('Начало прохождения', auto_now_add=True)
    finish = models.DateTimeField('Окончание прохождения', blank=True, null=True)
    finished = models.BooleanField('Завершен', default=False)
    verification = JSONField('Верификацция', blank=True, null=True)


    class Meta:
        verbose_name = 'Заполнение'
        verbose_name_plural = "Заполнения"
        unique_together = [['user_key', 'poll']]

    def __str__(self):
        return self.user_key

    def save(self, *args, **kwargs):
        if not self.pk:
            self.poll_data = self.calculate_questions_data()
            self.questions = self.calculate_question_list()
            self.question_count = len(self.questions)
            self.result_key = uuid.uuid4().hex[:8]
        if not self.result_key:
            self.result_key = uuid.uuid4().hex[:8]
        super(Answer, self).save(*args, **kwargs)

    def get_result_dict(self):

        d = {
            "email": self.email,
            "user_key": self.user_key,
            "result_key": self.result_key,
            "result_link": self.result_link,
            "poll": self.poll.name,
            "progress": self.progress,
            "start": self.start,
            "finish": self.finish,
            "finished": self.finished,
        }
        if self.result:
            for key in self.result:
                d[key + '_sum'] = self.result[key]['sum']
                d[key + '_average'] = self.result[key]['average']
                if key in QUARTILES:
                    d[key + '_quartile'] = get_quartile(key, self.result[key]['sum'])
                if key in PERCENTILES:
                    d[key + '_percentile'] = get_percentile(key, self.result[key]['average'])
            for key in specials.keys():
                try:
                    if key == 'SR':
                        d[key + '_sum'] = sum([self.result[specials[key][x]]['sum'] for x in range(len(specials[key]))])
                        d[key + '_average'] = max([self.result[specials[key][x]]['average'] for x in range(len(specials[key]))])
                        if key in QUARTILES:
                            d[key + '_quartile'] = get_quartile(key, sum([self.result[specials[key][x]]['sum'] for x in range(len(specials[key]))]))
                        if key in PERCENTILES:
                            d[key + '_percentile'] = get_percentile(key, max([self.result[specials[key][x]]['average'] for x in range(len(specials[key]))]))
                    else:
                        d[key + '_sum'] = sum([self.result[specials[key][x]]['sum'] for x in range(len(specials[key]))])
                        d[key + '_average'] = sum([self.result[specials[key][x]]['average'] for x in range(len(specials[key]))]) / len(specials[key])
                        if key in QUARTILES:
                            d[key + '_quartile'] = get_quartile(key, sum([self.result[specials[key][x]]['sum'] for x in range(len(specials[key]))]))
                        if key in PERCENTILES:
                            d[key + '_percentile'] = get_percentile(key, sum([self.result[specials[key][x]]['average'] for x in range(len(specials[key]))]) / len(specials[key]))
                except KeyError:
                    pass
        return d

    def verify_result(self, d):
        verification = {}
        try:
            if (d.get('ML_sum', 0) + d.get('IOT_sum', 0) + d.get('BCH_sum', 0) + d.get('CYB_sum', 0) + d.get('VR_sum', 0)) < 17 and d.get('IO_percentile', 0) >= 58:
                d['IO_percentile'] = 53
                verification['снижение IO при низк цифр грам'] = True
            if (d.get('ML_sum', 0) + d.get('IOT_sum', 0) + d.get('BCH_sum', 0) + d.get('CYB_sum', 0) + d.get('VR_sum', 0)) < 17 and d.get('IN_percentile', 0) >= 66:
                d['IN_percentile'] = int(d['IN_percentile'] * 0.9)
                verification['снижение IN при низк цифр грам'] = True
            if (d.get('ML_sum', 0) + d.get('IOT_sum', 0) + d.get('BCH_sum', 0) + d.get('CYB_sum', 0) + d.get('VR_sum', 0)) > 24 and d.get('IO_percentile', 0) <= 53:
                d['IO_percentile'] = 62
                verification['повышение IO при высок цифр грам'] = True
            if (d.get('ML_sum', 0) + d.get('IOT_sum', 0) + d.get('BCH_sum', 0) + d.get('CYB_sum', 0) + d.get('VR_sum', 0)) > 24 and d.get('IN_percentile', 0) <= 37:
                d['IN_percentile'] = 46
                verification['повышение IN при высок цифр грам'] = True
            # снижение TL при низк возр
            ans = self.poll_data
            for _ in ans:
                try:
                    if _['text'] == 'Укажите ваш возраст':
                        if int(_['answer']) <= 25 and d['TL_percentile'] >= 70:
                            d['TL_percentile'] = 62
                            verification['снижение TL при низк возр'] = True
                    elif _['text'] == 'Сколько человек у вас в прямом подчинении':
                        if _['answer'] == 1 and d['TL_percentile'] >= 79:
                            d['TL_percentile'] = 66
                            verification['снижение TL при 0 подчин'] = True
                        elif _['answer'] > 1 and d['TL_percentile'] <= 58:
                            d['TL_percentile'] = 66
                            verification['повыш TL при низк самооц'] = True
                except KeyError:
                    pass
        except ZeroDivisionError:
            pass
        finally:
            verification['verified'] = True
            return d, verification, True


    def get_result_dict_export(self):
        d = {}
        if self.result:
            for key in self.result:
                d[key + '_sum'] = self.result[key]['sum']
                d[key + '_average'] = self.result[key]['average']
                if key in QUARTILES:
                    d[key + '_quartile'] = get_quartile(key, self.result[key]['sum'])
                if key in PERCENTILES:
                    d[key + '_percentile'] = get_percentile(key, self.result[key]['average'])
            for key in specials.keys():
                try:
                    if key == 'SR':
                        d[key + '_sum'] = sum([self.result[specials[key][x]]['sum'] for x in range(len(specials[key]))])
                        d[key + '_average'] = max(
                            [self.result[specials[key][x]]['average'] for x in range(len(specials[key]))])
                        if key in QUARTILES:
                            d[key + '_quartile'] = get_quartile(key, sum(
                                [self.result[specials[key][x]]['sum'] for x in range(len(specials[key]))]))
                        if key in PERCENTILES:
                            d[key + '_percentile'] = get_percentile(key, max(
                                [self.result[specials[key][x]]['average'] for x in range(len(specials[key]))]))
                    else:
                        d[key + '_sum'] = sum([self.result[specials[key][x]]['sum'] for x in range(len(specials[key]))])
                        d[key + '_average'] = sum(
                            [self.result[specials[key][x]]['average'] for x in range(len(specials[key]))]) / len(
                            specials[key])
                        if key in QUARTILES:
                            d[key + '_quartile'] = get_quartile(key, sum(
                                [self.result[specials[key][x]]['sum'] for x in range(len(specials[key]))]))
                        if key in PERCENTILES:
                            d[key + '_percentile'] = get_percentile(key, sum(
                                [self.result[specials[key][x]]['average'] for x in range(len(specials[key]))]) / len(
                                specials[key]))
                except KeyError:
                    pass

        return d


    def calculate_questions_data(self):
        ql = []
        for s in self.poll.sections.filter(active=True, greeting_text__isnull=False):
            if s.greeting_text:
                ql.append({'name': s.name, 'greeting_text': s.greeting_text, 'type': 'instruction',
                           'answered': True, 'viewed': False, 'key': get_random_string(length=32)})

            # randomize order if necessary
            if s.random_order:
                qs = s.questions.filter(active=True).order_by('?')
            else:
                qs = s.questions.filter(active=True)
            i = 1
            for q in qs:
                ql.append({'pk': q.pk, 'type': q.type, 'text': q.text, 'answering_time': q.answering_time,
                           'progress': '{0} из {1}'.format(i, len(qs)),
                           'details': q.details, 'answered': False, 'viewed': False, 'answer': None,
                           'key': get_random_string(length=32), 'use_html': q.use_html, 'html_text': q.html_text})
                i = i + 1
        ql.append({'type': 'finish_text', 'finish_text': self.poll.finish_text, 'answered': True, 'viewed': False,
                   'key': get_random_string(length=32)})
        return ql

    def calculate_question_list(self):
        ql = []
        qs = Question.objects.filter(section__poll=self.poll, active=True)
        return [q.pk for q in qs]

    def get_question(self):
        try:
            q = [q for q in self.poll_data if not q['answered'] or not q['viewed']][0]
            q_i = self.poll_data.index(q)
            self.poll_data[q_i]['viewed'] = True
            self.save()
            if q['type'] == 'finish_text':
                self.finished = True
                self.finish = now()
                self.result_export = self.get_result_dict_export()
                self.save()
                self.calculate_result()
                self.send_webhook()
        except IndexError:
            q = self.poll_data[-1]
            self.finished = True
            self.finish = now()
            self.result_export = self.get_result_dict_export()
            self.save()
            self.calculate_result()
            self.send_webhook()
        return q

    def set_answer(self, question_pk, answer):
        q = [q for q in self.poll_data if q.get('pk', None) == question_pk][0]
        q_i = self.poll_data.index(q)
        self.poll_data[q_i]['answer'] = answer
        self.poll_data[q_i]['answered'] = True
        try:
            self.questions.remove(question_pk)
        except ValueError:
            pass
        self.progress = int(100 - len(self.questions)*100/self.question_count)
        self.save()
        self.send_webhook()
        return True

    def calculate_result(self):
        result = []
        for q in self.poll_data:
            if q.get('details', None):
                if q['details'].get('category', None):
                    c = q['details']['category']
                    points = 0
                    if type(q.get('answer', None)) == list:
                        for a in q.get('answer'):
                            options = q['details']['options']
                            questions = [x for x in options if x['id'] == a['id']]
                            question = questions[0]
                            points += question.get('points', 0)
                    elif type(q.get('answer', None)) == dict:
                        for a in q.get('answer'):
                            if q.get('answer')[a]:
                                options = q['details']['options']
                                questions = [x for x in options if x['id'] == int(a)]
                                question = questions[0]
                                points += question.get('points', 0)
                    elif type(q.get('answer', None)) == int:
                        a = q.get('answer')
                        options = q['details']['options']
                        questions = [x for x in options if x['id'] == a]
                        question = questions[0]
                        points += question.get('points', 0)
                    result.append({c: points})
        cts = []
        for i in result:
            for key in i.keys():
                cts.append(key)
        cts = list(set(cts))
        totals = {}
        for c in cts:
            totals[c] = {"items": [], "count": None, "average": None, "sum": None}

        for _ in result:
            key = [key for key in _][0]
            totals[key]['items'].append(_[key])

        for key in totals:
            totals[key]['count'] = len(totals[key]['items'])
            totals[key]['sum'] = sum(totals[key]['items'])
            totals[key]['average'] = sum(totals[key]['items']) / len(totals[key]['items'])

        self.result = totals
        result_export = self.get_result_dict_export()
        result_export, verification_data, verification_passed = self.verify_result(result_export)
        if verification_passed:
            self.verification = verification_data
        self.result_export = result_export
        self.save()
        return 'Success'

    def send_webhook(self):
        if self.poll.results_webhook and self.result:
            url = self.poll.results_webhook.format(id=self.user_key, slug=self.poll.slug, name=self.poll.name)
            headers = {'authorization': 'Token ' + self.poll.webhook_auth_token,
                       'content-type': 'application/json', 'accept': 'application/json'}
            r = post(url, headers=headers, data=dumps(
                {'digital_start': str(self.start),
                 'digital_finished': self.finished,
                 'digital_finish': str(self.finish),
                 'digital_progress': self.progress,
                 'digital_result_key': self.result_key,
                 'digital_result_basic': self.result.get('B', {}).get('sum', None),
                 'digital_result_ml': self.result.get('ML', {}).get('sum', None),
                 'digital_result_iot': self.result.get('IOT', {}).get('sum', None),
                 'digital_result_bch': self.result.get('BCH', {}).get('sum', None),
                 'digital_result_cyb': self.result.get('CYB', {}).get('sum', None),
                 'digital_result_vr': self.result.get('VR', {}).get('sum', None),
                 'digital_result_PST_syntesis': self.result.get('PST_syntesis', {}).get('sum', None),
                 'digital_result_PST_interpretation': self.result.get('PST_interpretation', {}).get('sum', None),
                 'digital_result_PST_search_hidden': self.result.get('PST_search_hidden_info', {}).get('sum', None),
                 }
            ))
            return r.text
        else:
            return 'No result, passing'

    def send_invitation(self):
        if self.email:
            poll = self.poll
            subject = "Пройдите тест Digital Index"
            txt_body = """Добрый день! Мы рады пригласить вас к участию в исследовании цифровой восприимчивости.
            Ваша персональная ссылка на SDI, Skolkovo Digital Index:
            https://digital-index.skolkovo.ru/#/?poll={0}&id={1} Общее время тестирования: 30-45 минут.
            Вы можете делать перерывы в процессе прохождения теста.
            Используйте ссылку для возвращения к тесту.""".format(poll.slug, self.user_key)
            html_raw = Template(poll.invitation)
            context = Context({'poll': poll, 'user_key': self.user_key})
            html_body = html_raw.render(context)
            from_email = "noreply@app.skolkovo.ru"
            to_email = [self.email]
            msg = EmailMultiAlternatives(subject, txt_body, from_email, to_email)
            msg.attach_alternative(html_body, "text/html")
            msg.send()
            return "Email успешно отправлен на адрес {0}".format(self.email)
        else:
            return "В заполнении {0} не указан email, отправка приглашения не удалась".format(self.user_key)


class UploadHandler(models.Model):
    file = models.FileField(verbose_name="Файл с участниками",
                           help_text="Файл должен соответствовать примеру - "
                                     "https://digital-back.skolkovo.ru/static/examples/upload_example.xlsx")
    poll = models.ForeignKey('Poll', verbose_name='Опрос', on_delete=models.CASCADE)
    created = models.DateTimeField(verbose_name="Дата загрузки", auto_now_add=True)
    progress = models.BooleanField(verbose_name="Обработано", default=False)
    log = models.TextField(verbose_name="Лог обработки", blank=True, null=True)

    class Meta:
        verbose_name = 'Загрузка участников'
        verbose_name_plural = "Загрузки участников"

    def __str__(self):
        return "{0} {1}".format(self.poll, self.created)


@receiver(post_save, sender=UploadHandler)
def create_participants(sender, instance, created, **kwargs):
    if created:
        df = pd.read_excel(instance.file)
        for i, r in df.iterrows():
            _ = Answer(user_key=r['user_key'], email=r['email'], poll=instance.poll)
            _.save()
            _.send_invitation()
            print(f"{r['email'], r['user_key']}")
        instance.progress = True
        instance.save()
