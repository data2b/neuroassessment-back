def get_quartile(key, sum):
    d = QUARTILES[key]
    keys = [x for x in d.keys()]
    for i in range(len(keys)):
        if i == len(keys):
            return d[keys[len(keys)]]
        elif i == 0:
            if sum <= keys[0]:
                return d[keys[0]]
        else:
            if sum <= keys[i] and sum > keys[i - 1]:
                return d[keys[i]]

def get_percentile(key, avg):
    d = PERCENTILES[key]
    dl = d.keys()
    nearest = min(dl, key=lambda x:abs(x-avg))
    return d[nearest]

QUARTILES = {
    'B': {
        6: 25,
        12: 50,
        19: 75,
        25.6: 100
    },
    'ML': {
        1: 25,
        4: 50,
        7: 75,
        9: 100
    },
    'INF': {
        1: 25,
        3: 50,
        5: 75,
        6: 100
    },
    'ECO': {
        1: 25,
        2: 50,
        3: 75,
        4: 100
    },
    'TRS': {
        1: 25,
        2: 50,
        4: 75,
        5: 100
    },
    'IOT': {
        2: 25,
        4: 50,
        6: 75,
        9: 100
    },
    'BCH': {
        2: 25,
        5: 50,
        6: 75,
        8: 100
    },
    'CYB': {
        2: 25,
        4: 50,
        6: 75,
        8: 100
    },
    'VR': {
        2: 25,
        4: 50,
        6: 75,
        8: 100
    },
}

PERCENTILES = {
    'I': {
        1.9: 1,
        2.0: 2,
        2.1: 3,
        2.2: 5,
        2.3: 7,
        2.40: 8,
        2.5: 9,
        2.6: 11,
        2.7: 13,
        2.8: 14,
        2.9: 15,
        3.0: 16,
        3.1: 19,
        3.2: 23,
        3.3: 29,
        3.4: 34,
        3.5: 38,
        3.60: 42,
        3.7: 47,
        3.80: 51,
        3.9: 54,
        4.00: 64,
        4.1: 68,
        4.20: 73,
        4.3: 77,
        4.40: 81,
        4.5: 83,
        4.60: 85,
        4.7: 88,
        4.80: 93,
        4.9: 96,
        5.00: 100
    },
    'IO': {
        1.9: 1,
        2.0: 2,
        2.1: 3,
        2.2: 4,
        2.3: 5,
        2.4: 6,
        2.5: 7,
        2.6: 8,
        2.7: 9,
        2.8: 10,
        2.9: 11,
        3.0: 14,
        3.1: 16,
        3.2: 19,
        3.3: 26,
        3.4: 32,
        3.5: 37,
        3.6: 41,
        3.7: 44,
        3.8: 48,
        3.9: 53,
        4.0: 58,
        4.1: 62,
        4.2: 67,
        4.3: 74,
        4.4: 79,
        4.5: 83,
        4.6: 88,
        4.7: 91,
        4.8: 94,
        4.9: 97,
        5.0: 100
    },
    'IN': {
        1.9: 3,
        2.0: 5,
        2.1: 7,
        2.2: 8,
        2.3: 9,
        2.4: 10,
        2.5: 11,
        2.6: 13,
        2.7: 15,
        2.8: 17,
        2.9: 19,
        3.0: 22,
        3.1: 27,
        3.2: 31,
        3.3: 34,
        3.4: 37,
        3.5: 39,
        3.6: 43,
        3.7: 46,
        3.8: 52,
        3.9: 59,
        4.0: 66,
        4.1: 68,
        4.2: 71,
        4.3: 75,
        4.4: 78,
        4.5: 84,
        4.6: 87,
        4.7: 90,
        4.8: 95,
        4.9: 98,
        5.0: 100
    },
    'IR': {
        1.9: 1,
        2.0: 2,
        2.1: 5,
        2.2: 7,
        2.3: 9,
        2.4: 16,
        2.5: 20,
        2.6: 25,
        2.7: 31,
        2.8: 38,
        2.9: 43,
        3.0: 47,
        3.1: 52,
        3.2: 54,
        3.3: 56,
        3.4: 58,
        3.5: 60,
        3.6: 62,
        3.7: 64,
        3.8: 66,
        3.9: 71,
        4.0: 75,
        4.1: 77,
        4.2: 82,
        4.3: 87,
        4.4: 89,
        4.5: 91,
        4.6: 95,
        4.7: 97,
        4.8: 98,
        4.9: 99,
        5.0: 100
    },
    'R': {
        1.9: 1,
        2.0: 2,
        2.1: 3,
        2.2: 4,
        2.3: 5,
        2.4: 6,
        2.5: 7,
        2.6: 8,
        2.7: 9,
        2.8: 10,
        2.9: 11,
        3.0: 14,
        3.1: 16,
        3.2: 19,
        3.3: 26,
        3.4: 32,
        3.5: 37,
        3.6: 41,
        3.7: 44,
        3.8: 48,
        3.9: 54,
        4.0: 61,
        4.1: 64,
        4.2: 69,
        4.3: 71,
        4.4: 75,
        4.5: 79,
        4.6: 83,
        4.7: 87,
        4.8: 94,
        4.9: 97,
        5.0: 100
    },
    'RF': {
        1.9: 1,
        2.0: 2,
        2.1: 3,
        2.2: 4,
        2.3: 5,
        2.4: 6,
        2.5: 7,
        2.6: 8,
        2.7: 11,
        2.8: 16,
        2.9: 22,
        3.0: 27,
        3.1: 31,
        3.2: 33,
        3.3: 35,
        3.4: 38,
        3.5: 41,
        3.6: 43,
        3.7: 46,
        3.8: 49,
        3.9: 53,
        4.0: 55,
        4.1: 62,
        4.2: 66,
        4.3: 69,
        4.4: 74,
        4.5: 78,
        4.6: 83,
        4.7: 87,
        4.8: 95,
        4.9: 98,
        5.0: 100
    },
    'RC': {
        1.9: 1,
        2.0: 2,
        2.1: 3,
        2.2: 5,
        2.3: 6,
        2.4: 7,
        2.5: 9,
        2.6: 13,
        2.7: 22,
        2.8: 28,
        2.9: 33,
        3.0: 39,
        3.1: 44,
        3.2: 48,
        3.3: 51,
        3.4: 55,
        3.5: 58,
        3.6: 61,
        3.7: 63,
        3.8: 65,
        3.9: 68,
        4.0: 71,
        4.1: 73,
        4.2: 76,
        4.3: 79,
        4.4: 81,
        4.5: 84,
        4.6: 87,
        4.7: 92,
        4.8: 95,
        4.9: 98,
        5.0: 100
    },
    'RL': {
        1.9: 1,
        2.0: 3,
        2.1: 4,
        2.2: 5,
        2.3: 6,
        2.4: 8,
        2.5: 10,
        2.6: 12,
        2.7: 13,
        2.8: 15,
        2.9: 19,
        3.0: 23,
        3.1: 27,
        3.2: 29,
        3.3: 33,
        3.4: 39,
        3.5: 43,
        3.6: 45,
        3.7: 48,
        3.8: 51,
        3.9: 59,
        4.0: 65,
        4.1: 69,
        4.2: 74,
        4.3: 79,
        4.4: 83,
        4.5: 86,
        4.6: 93,
        4.7: 95,
        4.8: 98,
        4.9: 99,
        5.0: 100
    },
    'S': {
        1.9: 1,
        2.00: 3,
        2.1: 7,
        2.2: 11,
        2.3: 15,
        2.40: 19,
        2.5: 22,
        2.6: 28,
        2.7: 31,
        2.8: 34,
        2.9: 38,
        3.0: 42,
        3.1: 45,
        3.2: 47,
        3.3: 51,
        3.4: 54,
        3.5: 58,
        3.60: 62,
        3.7: 65,
        3.80: 67,
        3.9: 69,
        4.00: 74,
        4.1: 76,
        4.20: 78,
        4.3: 81,
        4.40: 84,
        4.5: 87,
        4.60: 90,
        4.7: 92,
        4.80: 95,
        4.9: 98,
        5.00: 100
    },
    'SE': {
        1.9: 1,
        2.0: 3,
        2.1: 5,
        2.2: 8,
        2.3: 10,
        2.4: 11,
        2.5: 16,
        2.6: 21,
        2.7: 24,
        2.8: 29,
        2.9: 34,
        3.0: 39,
        3.1: 43,
        3.2: 48,
        3.3: 52,
        3.4: 55,
        3.5: 58,
        3.6: 62,
        3.7: 65,
        3.8: 67,
        3.9: 69,
        4.0: 72,
        4.1: 75,
        4.2: 81,
        4.3: 83,
        4.4: 85,
        4.5: 88,
        4.6: 90,
        4.7: 92,
        4.8: 95,
        4.9: 98,
        5.0: 100
    },
    'SR': {
        1.9: 1,
        2.0: 3,
        2.1: 5,
        2.2: 7,
        2.3: 9,
        2.4: 10,
        2.5: 16,
        2.6: 21,
        2.7: 29,
        2.8: 33,
        2.9: 37,
        3.0: 41,
        3.1: 45,
        3.2: 48,
        3.3: 52,
        3.4: 55,
        3.5: 58,
        3.6: 61,
        3.7: 64,
        3.8: 69,
        3.9: 72,
        4.0: 78,
        4.1: 79,
        4.2: 81,
        4.3: 84,
        4.4: 86,
        4.5: 87,
        4.6: 89,
        4.7: 91,
        4.8: 93,
        4.9: 97,
        5.0: 100
    },
    'SP': {
        1.9: 1,
        2.0: 4,
        2.1: 7,
        2.2: 9,
        2.3: 12,
        2.4: 16,
        2.5: 19,
        2.6: 23,
        2.7: 26,
        2.8: 30,
        2.9: 34,
        3.0: 37,
        3.1: 41,
        3.2: 44,
        3.3: 49,
        3.4: 52,
        3.5: 55,
        3.6: 59,
        3.7: 63,
        3.8: 68,
        3.9: 74,
        4.0: 77,
        4.1: 82,
        4.2: 85,
        4.3: 88,
        4.4: 90,
        4.5: 91,
        4.6: 93,
        4.7: 95,
        4.8: 97,
        4.9: 99,
        5.0: 100
    },
    'T': {
        1.9: 1,
        2.0: 3,
        2.1: 5,
        2.2: 7,
        2.3: 9,
        2.4: 14,
        2.5: 17,
        2.6: 20,
        2.7: 24,
        2.8: 27,
        2.9: 32,
        3.0: 35,
        3.1: 38,
        3.2: 42,
        3.3: 45,
        3.4: 48,
        3.5: 51,
        3.6: 55,
        3.7: 57,
        3.8: 59,
        3.9: 63,
        4.0: 67,
        4.1: 70,
        4.2: 73,
        4.3: 77,
        4.4: 81,
        4.5: 85,
        4.6: 87,
        4.7: 90,
        4.8: 93,
        4.9: 96,
        5.0: 100
    },
    'TA': {
        1.9: 1,
        2.0: 3,
        2.1: 5,
        2.2: 8,
        2.3: 11,
        2.4: 16,
        2.5: 19,
        2.6: 25,
        2.7: 28,
        2.8: 34,
        2.9: 38,
        3.0: 40,
        3.1: 44,
        3.2: 47,
        3.3: 51,
        3.4: 53,
        3.5: 55,
        3.6: 58,
        3.7: 61,
        3.8: 65,
        3.9: 69,
        4.0: 73,
        4.1: 75,
        4.2: 78,
        4.3: 82,
        4.4: 86,
        4.5: 89,
        4.6: 92,
        4.7: 95,
        4.8: 98,
        4.9: 99,
        5.0: 100
    },
    'TE': {
        1.9: 1,
        2.0: 3,
        2.1: 6,
        2.2: 8,
        2.3: 11,
        2.4: 14,
        2.5: 17,
        2.6: 20,
        2.7: 24,
        2.8: 28,
        2.9: 33,
        3.0: 36,
        3.1: 39,
        3.2: 41,
        3.3: 44,
        3.4: 47,
        3.5: 50,
        3.6: 52,
        3.7: 55,
        3.8: 59,
        3.9: 63,
        4.0: 67,
        4.1: 71,
        4.2: 74,
        4.3: 77,
        4.4: 80,
        4.5: 84,
        4.6: 87,
        4.7: 90,
        4.8: 93,
        4.9: 97,
        5.0: 100
    },
    'TL': {
        1.9: 1,
        2.0: 3,
        2.1: 4,
        2.2: 5,
        2.3: 6,
        2.4: 8,
        2.5: 12,
        2.6: 18,
        2.7: 23,
        2.8: 28,
        2.9: 32,
        3.0: 35,
        3.1: 38,
        3.2: 41,
        3.3: 44,
        3.4: 47,
        3.5: 49,
        3.6: 52,
        3.7: 55,
        3.8: 58,
        3.9: 62,
        4.0: 66,
        4.1: 70,
        4.2: 73,
        4.3: 76,
        4.4: 79,
        4.5: 82,
        4.6: 85,
        4.7: 89,
        4.8: 93,
        4.9: 96,
        5.0: 100
    }
}
