from django.apps import AppConfig


class PollsConfig(AppConfig):
    name = 'neuroassessment_backend.polls'
    verbose_name = 'Опросы и тесты'
