# Generated by Django 2.2.5 on 2019-11-14 00:02

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0021_auto_20191113_2351'),
    ]

    operations = [
        migrations.AddField(
            model_name='poll',
            name='allow_all',
            field=models.BooleanField(default=True, help_text='Если не отмечена - для прохождения надо иметь id из списка доступных', verbose_name='Разрешать прохождение с любым id'),
        ),
        migrations.AddField(
            model_name='poll',
            name='allowed_ids',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Допустимые id'),
        ),
    ]
