# Generated by Django 2.2.5 on 2019-11-11 22:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0018_auto_20191111_0308'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='result_key',
            field=models.CharField(blank=True, max_length=20, null=True, unique=True, verbose_name='Токен доступа к результатам'),
        ),
    ]
