# Generated by Django 2.2.5 on 2019-10-11 22:42

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0008_auto_20191011_2216'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='question_count',
            field=models.IntegerField(default=0, verbose_name='Количество вопросов'),
        ),
        migrations.AddField(
            model_name='answer',
            name='questions',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Вопросы'),
        ),
    ]
